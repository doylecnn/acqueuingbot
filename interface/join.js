// Frameworks
const debug = require('debug')('acqb:interface/join.js')
const uuid = require('uuid')

// Database plugin
const database = require('../plugins/database')

module.exports = async (queue, joiner) => {
  debug(queue, joiner)

  // Transfer telegram intid into uuid
  let sth = await database('r', `SELECT * FROM user WHERE tgid = '${joiner}'`)
  joiner = sth[0]
  
  // Check if this user already joined any queue or inited a queue
  sth = await database('r', `SELECT id FROM member WHERE user = '${joiner.id}' AND (status = 0 OR status = 1)`)
  if (sth.length) return { joinmsg: '你已加入某个队列。在加入新的队列之前，你需要先使用 /end 指令从现有队列退出。' }
  sth = await database('r', `SELECT id FROM queue WHERE initiator = '${joiner.id}' AND status = 0`)
  if (sth.length) return { joinmsg: '你已发起一个队列，在加入新的队列之前，你需要等待该队列所有人完成联机以解散该队列，或使用 /destroy 强制结束队列。' }

  // Check is queue available, and get queue meta info
  queue = await database('r', `SELECT * FROM queue WHERE id = '${queue}'`)
  if (!queue.length) return '没有找到队列。'
  queue = queue[0]
  debug(queue)
  if (queue.status !== 0) return { joinmsg: '你请求加入的队列已经失效。' }

  // Get initiator's Telegram intid
  let initiator = await database('r', `SELECT * FROM user WHERE id = '${queue.initiator}'`)
  initiator = initiator[0]

  // Check how many people waiting at this queue and not in hold
  sth = await database('r', `SELECT id FROM member WHERE queue = '${queue.id}' AND status = 1`)
  if (sth.length < queue.max) {
    // Join directly
    await database('w', `INSERT INTO member (id, user, queue, status, jointime) VALUES ('${uuid.v4()}', '${joiner.id}', '${queue.id}', 1, '${new Date().getTime()}')`)
    return {
      joinmsg: `你已加入 ${initiator.firstname}${initiator.tgusername ? (' (@' + initiator.tgusername + ')') : ''} 的队列，由于当前队列中尚有联机空位，你可以立即进行联机。\n联机密码：\`${queue.password}\`。\n点击 /end 结束联机。`,
      initiator: {
        id: initiator.tgid,
        msg: `*有人加入你的队列*\n联机用户：${joiner.firstname} ${joiner.tgusername ? (' (@' + joiner.tgusername + ')') : ''}\n由于当前队列中尚有联机空位，机器人已将联机密码直接提供给对方。`
      }
    }
  } else {
    // Join and wait
    await database('w', `INSERT INTO member (id, user, queue, status, jointime) VALUES ('${uuid.v4()}', '${joiner.id}', '${queue.id}', 0, '${new Date().getTime()}')`)
    return {
      joinmsg: `你已加入 ${initiator.firstname}${initiator.tgusername ? (' (@' + initiator.tgusername + ')') : ''} 的队列，请等待队列移动。当联机可用时，机器人将会向你发送联机密码。\n/end 从队列中退出。\n/hold 暂离队列。\n/list 查看队列情况。`,
      initiator: {
        id: initiator.tgid,
        msg: `*有人加入你的队列*\n联机用户：${joiner.firstname} ${joiner.tgusername ? (' (@' + joiner.tgusername + ')') : ''}`
      }
    }
  }
}
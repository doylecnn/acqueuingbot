// Frameworks
const debug = require('debug')('acqb:interface/new.js')
const uuid = require('uuid')

// Database plugin
const database = require('../plugins/database')

module.exports = async (id, password, max) => {
  // Verify input
  if (!password || !max) {
    let res = ''
    res += '*生成新队列*\n'
    res += '请发送 `/new <联机密码> <最多同时访问人数>` 指令来生成新队列。'
    return { helpinfo: res }
  }

  // Verify format of password
  if (password.length !== 5) {
    let res = ''
    res += '*密码格式不正确*\n'
    res += '正确的密码应该是五位数字、英文字母（除 I、O、Z 外）组合。'
    return { helpinfo: res }
  }

  // Verify max member
  if (isNaN(max) || max < 1 || max > 7) {
    let res = ''
    res += '*最多同时访问人数不正确*\n'
    res += '最低 1 人联机、最多 7 人联机。建议将此值设为 2。'
    return { helpinfo: res }
  }

  // Is user already have a queue or joined a queue?
  let db = await database('r', `SELECT id, tgusername FROM user WHERE tgid = '${id}'`)
  if (!db.length) return { helpinfo: '*发生严重错误*\n无法在数据库中找到你的资料。尝试重新发送 /start 并再次尝试此操作。' }
  let user = db[0]
  db = await database('r', `SELECT * FROM queue WHERE initiator = '${user.id}' AND status = 0`)
  if (db.length) return { helpinfo: '*你已发起一个队列*\n在发起新队列时，需要先取消当前队列。' }
  db = await database('r', `SELECT * FROM member WHERE user = '${user.id}' AND (status = 0 OR status = 1 OR status = 2)`)
  if (db.length) return { helpinfo: '*你已加入一个队列*\n在发起新队列时，需要先用 /end 指令从当前队列中退出等待。' }

  // Write queue information into database
  await database('w', `INSERT INTO queue (id, initiator, password, max, status) VALUES ('${uuid.v4()}', '${user.id}', '${password}', ${max}, 0)`)

  // Generate feedback
  let helpinfo = ''
  helpinfo += '队列已就绪，等待加入。\n/dismiss 解散该队列。\n/list 查看队列情况。\n/update 更新联机密码、最大人数等信息。'
  let attachinfo = {
    reply_markup: [[{
      text: '选择聊天以邀请...',
      switch_inline_query: ''
    }]]
  }
  return {helpinfo, attachinfo}
}
// Frameworks
const debug = require('debug')('acqb:interface/list.js')

// Database plugin
const database = require('../plugins/database')

module.exports = async (user) => {
  // Transfer tgid into actually user
  user = await database('r', `SELECT * FROM user WHERE tgid = '${user}'`)
  user = user[0]

  // Find which queue does this user want
  let sth = await database('r', `SELECT * FROM queue WHERE initiator = '${user.id}' AND status = 0`)
  if (!sth.length) {
    sth = await database('r', `SELECT * FROM member WHERE user = '${user.id}' AND (status = 0 OR status = 1 OR status = 2)`)
    if (!sth.length) return '没有可供查看的队列列表。'
    sth = await database('r', `SELECT * FROM queue WHERE id = '${sth[0].queue}'`)
  }
  let queue = sth[0]

  // Fetch queue members
  debug('====')
  debug(queue.id)
  let members = await database('r', `SELECT * FROM member WHERE queue = '${queue.id}' AND (status = 0 OR status = 1 OR status = 2) ORDER BY jointime`)
  debug(members)

  // Transfer list into text
  let membersInText = ''
  let youAreHere
  let holding = 0
  for (let i in members) {
    // Fetch profile
    let profile = await database('r', `SELECT * FROM user WHERE id = '${members[i].user}'`)
    profile = profile[0]

    // Write status
    membersInText += `${members[i].status === 0 ? '⏳' : ''}${members[i].status === 1 ? '▶️' : ''}${members[i].status === 2 ? '🆖' : ''} `

    // Write name
    membersInText += `${profile.firstname}${profile.tgusername ? (' (@' + profile.tgusername + ')') : ''} `
    
    // If found this user
    if (user.id === profile.id) {
      membersInText += `🙋‍♀️`
      youAreHere = parseInt(i) + 1 
      debug('youAreHere = ' + youAreHere)
    }

    // If this item is holding
    if (members[i].status === 2) holding++

    // Insert \n
    membersInText += `\n`
  }

  let header = `当前有 ${members.length} 位用户在队列中`
  if (holding) header += `，其中有 ${holding} 位用户暂离`
  if (youAreHere) header += `，你在其中第 ${youAreHere} 位`
  header += `。\n`
  
  return header + membersInText
}
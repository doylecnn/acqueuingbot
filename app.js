// Frameworks
const Koa = require('koa')
const app = new Koa()
const Telegraf = require('telegraf')
const Telegram = require('telegraf/telegram')
const debug = require('debug')('acqb:app.js')
const koaBody = require('koa-body')
app.use(koaBody())

// Config
const config = require('./config.json')

// Interface
const interface = require('./interface')

// Bot initialition
const telegrafbot = new Telegraf(config.telegram.token)
const telegrambot = new Telegram(config.telegram.token, {
  agent: null,
  webhookReply: true
})
app.use(async (ctx, next) => {
  if (ctx.method !== 'POST' || ctx.url !== `/${config.meta.random}`) return next()
  await telegrafbot.handleUpdate(ctx.request.body, ctx.response)
  ctx.status = 200
})
telegrafbot.telegram.setWebhook(config.meta.domain + config.meta.random)

// Commands and routers
telegrafbot.start(async ctx => {
  let update = ctx.update
  if (!update.message.text.split(' ')[1]) {
    let sth
    try {
      sth = await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    } catch (e) {
      debug(e)
      return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
    }
    telegrambot.sendMessage(update.message.chat.id, sth, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  } else {
    let sth
    try {
      await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
      sth = await interface.join(update.message.text.split(' ')[1], update.message.chat.id)
    } catch (e) {
      debug(e)
      return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
    }
    telegrambot.sendMessage(update.message.chat.id, sth.joinmsg, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
    if (sth.initiator) {
      telegrambot.sendMessage(sth.initiator.id, sth.initiator.msg, { parse_mode: 'Markdown' })
    }
  }
})

telegrafbot.command('new', async ctx => {
  let update = ctx.update
  let sth
  try {
    await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    sth = await interface.new(update.message.from.id, update.message.text.split(' ')[1], parseInt(update.message.text.split(' ')[2]))
  } catch (e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  }
  debug(sth.attachinfo)
  if (sth.attachinfo)  telegrambot.sendMessage(update.message.chat.id, sth.helpinfo, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown', reply_markup: { inline_keyboard: sth.attachinfo.reply_markup } })
  else telegrambot.sendMessage(update.message.chat.id, sth.helpinfo, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
})

telegrafbot.command('end', async ctx => {
  let update = ctx.update
  let sth
  try {
    await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    sth = await interface.end(update.message.from.id)
  } catch (e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  }
  telegrambot.sendMessage(update.message.chat.id, sth.requester, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  if (sth.initiator) telegrambot.sendMessage(sth.initiator.tgid, sth.initiator.msg, {parse_mode: 'Markdown' })
  if (sth.next) telegrambot.sendMessage(sth.next.tgid, sth.next.msg, {parse_mode: 'Markdown' })
})

telegrafbot.command('hold', async ctx => {
  let update = ctx.update
  let sth
  try {
    await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    sth = await interface.hold(update.message.from.id)
  } catch (e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  }
  telegrambot.sendMessage(update.message.chat.id, sth.requestormsg, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  if (sth.initiator) telegrambot.sendMessage(sth.initiator.tgid, sth.initiator.msg, { parse_mode: 'Markdown' })
})

telegrafbot.command('dismiss', async ctx => {
  let update = ctx.update
  let sth
  try {
    await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    sth = await interface.dismiss(update.message.from.id)
  } catch (e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  }
  telegrambot.sendMessage(update.message.chat.id, sth.requestormsg , { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  if (!sth.members) return
  for (let i in sth.members.list) {
    telegrambot.sendMessage(sth.members.list[i].tgid, sth.members.message , { parse_mode: 'Markdown' })
  }
})

telegrafbot.command('list', async ctx => {
  let update = ctx.update
  let sth
  try {
    await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    sth = await interface.list(update.message.from.id)
  } catch (e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  }
  telegrambot.sendMessage(update.message.chat.id, sth, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
})

telegrafbot.command('update', async ctx => {
  let update = ctx.update
  let sth
  try {
    await interface.start(update.message.from.id, update.message.from.first_name, update.message.from.last_name, update.message.from.username)
    sth = await interface.update(update.message.from.id, update.message.text.split(' ')[1], parseInt(update.message.text.split(' ')[2]))
  } catch (e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '出现意外错误，请联系管理员进行诊断。', { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  }
  telegrambot.sendMessage(update.message.chat.id, sth.requestor, { reply_to_message_id: update.message.message_id, parse_mode: 'Markdown' })
  if (sth.members) {
    for (let i in sth.members.list) telegrambot.sendMessage(sth.members.list[i], sth.members.msg, { parse_mode: 'Markdown' })
  }
})

// Inline share
telegrafbot.on('inline_query', async (ctx) => {
  let query = ctx.update.inline_query
  debug(query)
  let res = []
  let sth
  try {
    await interface.start(query.from.id, query.from.first_name, query.from.last_name, query.from.username)
    sth = await interface.share(query.from.id)
  } catch(e) {
    debug(e)
    return telegrambot.sendMessage(update.message.chat.id, '使用内联模式时出现意外错误，请联系管理员进行诊断。', { parse_mode: 'Markdown' })
  }
  if (sth.available) {
    res[res.length] = {
      type: 'article',
      id: '0',
      title: '点此在该聊天发送联机邀请函',
      description: '以邀请与你联机……',
      input_message_content: {
        message_text: '要到我的动森无人岛的水友，请往这边排队～',
        parse_mode: 'Markdown'
      },
      reply_markup: { inline_keyboard: [[{
        text: `排队`,
        url: `https://t.me/${config.telegram.username}?start=${sth.queueid}`
      }]] }
    }
  }
  res[res.length] = {
    type: 'article',
    id: '1',
    title: '点此向群友安利本机器人',
    input_message_content: {
      message_text: `来尝试一下 [动森叫号器](https://t.me/${config.telegram.username})，帮你管理动森联机水友～`,
      parse_mode: 'Markdown'
    }
  }
  telegrambot.answerInlineQuery(query.id, res, { cache_time: 0})
})

app.listen(3000)
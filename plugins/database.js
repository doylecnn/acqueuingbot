const debug = require('debug')('acqb:modules/sqlite.js')
const sqlite = require('sqlite-async')

// Import file
let db
sqlite.open('database.db')
  .then(database => {
    db = database
  })
  .catch(err => {
    debug(err)
    throw 'an error occured in connect to database'
  })

module.exports = async (ops, handle) => {
  if (ops === 'w') return await db.run(handle)
  else if (ops === 'r') return await db.all(handle)
  else throw 'operation type error'
}